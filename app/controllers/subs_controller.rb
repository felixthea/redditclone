class SubsController < ApplicationController
  before_filter :require_current_user, except: [:index, :show]

  def index
    @links = Kaminari.paginate_array(Link.all.sort_by(&:popularity).reverse).page(params[:page]).per(25)

    render :index
  end

  def show
    @sub = Sub.find(params[:id])
    @links = Kaminari.paginate_array(@sub.links.sort_by(&:popularity).reverse).page(params[:page]).per(25)
    render :show
  end

  def new
    render :new
  end

  def create
    @sub = current_user.subs.new(params[:sub])
    if @sub.save
      redirect_to @sub
    else
      flash[:errors] = @sub.errors.full_messages
      render :new
    end
  end

  def edit
    @sub = Sub.find(params[:id])
    render :edit
  end

  def update
    @sub = Sub.find(params[:id])
    if current_user.id == @sub.moderator.id
      if @sub.update_attributes(params[:sub])
        redirect_to @sub
      else
        flash[:errors] = @sub.errors.full_messages
        render :edit
      end
    else
      flash[:errors] = "You can only edit this sub if you're the moderator"
      redirect_to @sub
    end
  end
end

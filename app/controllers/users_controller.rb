class UsersController < ApplicationController
  before_filter :require_current_user, only: [:show]
  def new
    render :new
  end

  def show
    @user = current_user
    render :show
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      log_in!(@user)
      redirect_to @user
    else
      flash[:errors] = @user.errors.full_messages
      render :new
    end
  end
end

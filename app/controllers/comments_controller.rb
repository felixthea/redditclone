class CommentsController < ApplicationController
  def create
    @comment = current_user.comments.new(params[:comment])
    if @comment.save
      redirect_to @comment.link
    else
      flash[:errors] = @comment.errors.full_messages
      redirect_to :back
    end
  end
end

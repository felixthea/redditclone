class LinksController < ApplicationController
  before_filter :require_current_user, only: [:new, :create, :edit, :destroy, :update, :upvote, :downvote]
  before_filter :current_link, only: [:show, :edit, :update, :destroy]
  before_filter :is_submitter, only: [:edit, :destroy, :update]

  def new
    @link = Link.new
  end

  def create
    @link = current_user.links.new(params[:link])
    unless params[:comment][:body].blank?
      comment = @link.comments.new(params[:comment])
      comment.author = current_user
    end
    if @link.save
      redirect_to @link
    else
      flash[:errors] = @link.errors.full_messages
      render :new
    end
  end

  def edit
  end

  def show
    @comments = @link.comments_by_parent_id
  end

  def update
    to_delete = @link.comment_ids - params[:link][:comment_ids]
    Comment.where(id: to_delete).destroy_all
    if @link.update_attributes(params[:link])
      redirect_to @link
    else
      flash[:errors] = @links.errors.full_messages
      render :edit
    end
  end

  def destroy
    @link.destroy
    redirect_to links_url
  end

  def upvote
    @link = Link.find(params[:link_id])
    vote = current_user.votes.new(vote_type: 1, link: @link)
    if vote.save
      redirect_to :back
    else
      flash[:errors] = ['You already voted on this link']
      redirect_to :back
    end
  end

  def downvote
    @link = Link.find(params[:link_id])
    vote = current_user.votes.new(vote_type: -1, link: @link)
    if vote.save
      redirect_to :back
    else
      flash[:errors] = ['You already voted on this link']
      redirect_to :back
    end
  end

  private
  def current_link
    @link ||= Link.find(params[:id])
  end

  def is_submitter
    redirect_to @link unless @link.submitter.id == current_user.id
  end
end

class Sub < ActiveRecord::Base
  attr_accessible :name, :moderator

  validates :name, :moderator, presence: true
  validates :name, uniqueness: true

  belongs_to(
    :moderator,
    class_name: 'User',
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :subs
  )

  has_many(
    :sub_links,
    class_name: 'SubLink',
    foreign_key: :sub_id,
    primary_key: :id,
    inverse_of: :sub
  )

  has_many :links, through: :sub_links, source: :link
end

class Link < ActiveRecord::Base
  attr_accessible :description, :title, :url, :submitter, :sub_ids, :comment_ids

  validates :title, :url, :submitter, presence: true

  belongs_to(
    :submitter,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :links
  )

  has_many(
    :sub_links,
    class_name: 'SubLink',
    foreign_key: :link_id,
    primary_key: :id,
    inverse_of: :link
  )

  has_many(
    :comments,
    class_name: 'Comment',
    foreign_key: :link_id,
    primary_key: :id,
    inverse_of: :link
  )

  has_many(
    :votes,
    class_name: "UserVote",
    foreign_key: :link_id,
    primary_key: :id,
    inverse_of: :link
  )

  has_many :subs, through: :sub_links, source: :sub

  def comments_by_parent_id
    comment_hash = {}
    comments.each do |comment|
      comment_hash[comment.parent_comment_id] ||= []
      comment_hash[comment.parent_comment_id] += [comment]
    end

    comment_hash
  end

  def popularity
    votes.sum(:vote_type)
  end
end

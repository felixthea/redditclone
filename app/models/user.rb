require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :username, :password
  attr_reader :password

  validates :username, :session_token, presence: true, uniqueness: true
  validates :password_digest, presence: { message: "Password can't be blank" }
  validates :password, length: { minimum: 6 }

  after_initialize :ensure_session_token

  has_many(
    :subs,
    class_name: 'Sub',
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :moderator
  )

  has_many(
    :links,
    class_name: "Link",
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :submitter
  )

  has_many(
    :comments,
    class_name: 'Comment',
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :author
  )

  has_many(
    :votes,
    class_name: "UserVote",
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :user
  )

  def ensure_session_token
    self.session_token ||= self.class.generate_token
  end

  def password=(pwd)
    @password = pwd
    unless pwd.blank?
      self.password_digest = BCrypt::Password.create(pwd)
    end
  end

  def reset_session_token
    self.session_token = self.class.generate_token
    self.save!

    self.session_token
  end

  def has_password?(pwd)
    BCrypt::Password.new(self.password_digest).is_password?(pwd)
  end

  def self.find_by_credentials(username, pwd)
    user = User.find_by_username(username)

    return nil if user.nil?

    if user.has_password?(pwd)
      user
    else
      nil
    end
  end

  private
  def self.generate_token
    SecureRandom.urlsafe_base64(16)
  end
end

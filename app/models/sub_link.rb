class SubLink < ActiveRecord::Base
  attr_accessible :link, :sub

  validates :link, :sub, presence: true

  belongs_to(
    :link,
    class_name: 'Link',
    foreign_key: :link_id,
    primary_key: :id,
    inverse_of: :sub_links
  )

  belongs_to(
    :sub,
    class_name: 'Sub',
    foreign_key: :sub_id,
    primary_key: :id,
    inverse_of: :sub_links
  )
end

class UserVote < ActiveRecord::Base
  attr_accessible :user, :link, :vote_type

  validates :user, :link, :vote_type, presence: true
  validates :user_id, uniqueness: { scope: :link_id }
  validates :vote_type, inclusion: { in: [1, -1] }

  belongs_to(
    :user,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :votes
  )

  belongs_to(
    :link,
    class_name: "Link",
    foreign_key: :link_id,
    primary_key: :id,
    inverse_of: :votes
  )
end

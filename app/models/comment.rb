class Comment < ActiveRecord::Base
  attr_accessible :author, :parent_comment_id, :link_id, :body

  validates :author, :link, :body, presence: true

  belongs_to(
    :author,
    class_name: 'User',
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :comments
  )

  belongs_to(
    :link,
    class_name: 'Link',
    foreign_key: :link_id,
    primary_key: :id,
    inverse_of: :comments
  )

  belongs_to(
    :parent_comment,
    class_name: 'Comment',
    foreign_key: :parent_comment_id,
    primary_key: :id,
    inverse_of: :child_comments
  )

  has_many(
    :child_comments,
    class_name: "Comment",
    foreign_key: :parent_comment_id,
    primary_key: :id,
    inverse_of: :parent_comment,
    dependent: :destroy
  )
end

class CreateSubs < ActiveRecord::Migration
  def change
    create_table :subs do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
    add_index :subs, :user_id
  end
end

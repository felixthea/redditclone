require 'spec_helper'

describe UserVote do
  it { should validate_presence_of(:link) }
  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:vote_type) }

  it "validates that the combination of user and link is unique" do
    first_vote = FactoryGirl.create(:user_voted)
    vote = FactoryGirl.build(:user_vote, link: first_vote.link, vote_type: "Up", user: first_vote.user)

    expect(vote).not_to be_valid
  end

  it { should allow_mass_assignment_of(:link) }
  it { should allow_mass_assignment_of(:user) }
  it { should allow_mass_assignment_of(:vote_type) }

  it { should belong_to(:link) }
  it { should belong_to(:user) }

  it { should ensure_inclusion_of(:vote_type).in_array([1, -1])}
end

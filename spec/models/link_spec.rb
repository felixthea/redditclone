require 'spec_helper'
require 'faker'

describe Link do
  let(:incomplete_link) { FactoryGirl.build(:link) }

  it { should validate_presence_of(:title) }

  it { should validate_presence_of(:url) }

  it { should_not validate_presence_of(:description) }

  it "fails validation with no title expecting a specific message" do
    expect(incomplete_link.errors_on(:title)).to include("can't be blank")
  end

  it "fails validation with no url expecting a specific message" do
    expect(incomplete_link.errors_on(:url)).to include("can't be blank")
  end

  it { should belong_to(:submitter) }
  it { should have_many(:sub_links) }
  it { should have_many(:subs) }
  it { should have_many(:comments) }

  it { should allow_mass_assignment_of(:submitter) }
  it { should allow_mass_assignment_of(:title) }
  it { should allow_mass_assignment_of(:url) }
  it { should allow_mass_assignment_of(:description) }
  it { should allow_mass_assignment_of(:sub_ids) }

  context "#comments_by_parent_id" do
    it "returns comments nested within their parent ids" do
      user = User.create!(username: Faker::Internet.user_name, password: '123456')
      sub = user.subs.create!(name: Faker::Internet.user_name)
      link = Link.create!(title: 'what is ruby', url:'ruby-lang.org', description: 'cool', sub_ids: [sub.id], submitter: User.create!(username: Faker::Internet.user_name, password: '123456'))
      comments = FactoryGirl.create_list(:comment_with_no_associations, 3, link: link, author: user)
      comments.each do |comment|
        FactoryGirl.create_list(:comment_with_no_associations, 3, link: link, parent_comment: comment, author: User.create!(username: Faker::Internet.user_name, password: '123456'))
      end

      link.comments_by_parent_id.each do |k, v|
        expect(v.all? { |c| c.parent_comment_id == k }).to be_true
      end
    end
  end
end

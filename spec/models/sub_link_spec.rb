require 'spec_helper'

describe SubLink do
  let(:incomplete_sub_link) { FactoryGirl.build(:sub_link) }

  it { should validate_presence_of(:sub) }

  it { should validate_presence_of(:link) }

  it { should belong_to(:sub) }

  it { should belong_to(:link) }
end

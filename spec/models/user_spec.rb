require 'spec_helper'

describe User do
  let(:incomplete_user) { FactoryGirl.build(:user) }

  it "validates presence of username" do
    expect(incomplete_user).to have(1).error_on(:username)
  end

  it "validates presence of password digest" do
    expect(incomplete_user).to have(1).error_on(:password_digest)
  end

  it "validates the length of password is greater than 6" do
    expect(incomplete_user).to have(1).error_on(:password)
  end

  it "fails validation with no username expecting a specific message" do
    expect(incomplete_user.errors_on(:username)).to include("can't be blank")
  end

  it "fails validation with no password expecting a specific message" do
    expect(incomplete_user.errors_on(:password)).to include("is too short (minimum is 6 characters)")
  end

  it "validates that username is unique" do
    user1 = FactoryGirl.create(:complete_user)
    user2 = FactoryGirl.build(:repeat_user)

    expect(user2).not_to be_valid
  end

  it "should always have a session token" do
    expect(incomplete_user.session_token).not_to be_nil
  end

  it { should allow_mass_assignment_of(:password) }

  it { should allow_mass_assignment_of(:username) }

  it { should_not allow_mass_assignment_of(:session_token) }

  it "#reset_session_token should change the token" do
    user = FactoryGirl.create(:complete_user)
    old_token = user.session_token
    new_token = user.reset_session_token

    expect(new_token).not_to eql old_token
  end

  it "should return the user if credentials match" do
    FactoryGirl.create(:complete_user)
    user = User.find_by_credentials("User", "123456")

    expect(user).not_to be_nil
  end

  it "should return nil if the credentials do not match" do
    FactoryGirl.create(:complete_user)
    user = User.find_by_credentials("Something_Else", "654321")

    expect(user).to be_nil
  end

  it "should not store the password" do
    user = FactoryGirl.create(:complete_user)
    User.find(user.id).password.should be_nil
  end

  it { should have_many(:subs) }

  it { should have_many(:links) }

  it { should have_many(:comments) }
end

require 'spec_helper'

describe Sub do
  let(:incomplete_sub) { FactoryGirl.build(:sub) }

  it "validates presence of name" do
    expect(incomplete_sub).to have(1).error_on(:name)
  end

  it "validates uniqueness of name" do
    FactoryGirl.create(:complete_sub)
    sub = FactoryGirl.build(:complete_sub)
    expect(sub).not_to be_valid
  end

  it "fails validation with no name expecting a specific message" do
    expect(incomplete_sub.errors_on(:name)).to include("can't be blank")
  end

  it "fails validation with not unique name expecting a specific message" do
    FactoryGirl.create(:complete_sub)
    sub = FactoryGirl.build(:complete_sub)
    expect(sub.errors_on(:name)).to include("has already been taken")
  end

  it { should belong_to(:moderator) }
  it { should have_many(:sub_links) }
  it { should have_many(:links) }

  it { should allow_mass_assignment_of(:name) }

  it { should allow_mass_assignment_of(:moderator) }
end

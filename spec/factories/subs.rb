# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :sub do
    factory :complete_sub do
      name Faker::Name.name
      association :moderator, factory: :complete_user, password: "1234567", username: Faker::Name.name, strategy: :build
    end
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :user do
    factory :complete_user do
      username "User"
      password "123456"
      initialize_with { User.find_or_create_by_username(username) }
    end

    factory :repeat_user do
      username "User"
      password "123456"
    end


    factory :random_user do
      id 1000
      username Faker::Internet.user_name
      password '123456'
      initialize_with { User.find_or_create_by_username(username) }
   end
  end
end

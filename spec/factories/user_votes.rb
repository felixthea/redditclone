# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_vote do
    factory :user_voted do
      association :link, factory: :complete_link, strategy: :build
      association :user, factory: :random_user, strategy: :build
      vote_type 1
    end
  end
end

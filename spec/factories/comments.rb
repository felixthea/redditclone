# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :comment do

    factory :complete_comment do
      body Faker::Lorem.paragraph
      association :author, factory: :complete_user,
        username: Faker::Internet.user_name,
        password: Faker::Internet.password,
        strategy: :build
      association :link, factory: :complete_link, strategy: :build

      factory :complete_comment_with_parent do
        association :parent_comment, factory: :complete_comment
      end
    end

    factory :comment_with_no_associations do
      body Faker::Lorem.paragraph
    end
  end
end

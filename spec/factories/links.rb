# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :link do
    factory :complete_link do
      title Faker::Lorem.sentence
      url Faker::Internet.domain_name
      description Faker::Lorem.paragraph
      association :submitter, factory: :user,
        username: Faker::Internet.name,
        password: Faker::Internet.password
    end
  end
end

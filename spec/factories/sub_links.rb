# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sub_link do
    factory :complete_sub_link do
      association :link, factory: :complete_link
      association :sub, factory: :complete_sub
    end
  end
end
